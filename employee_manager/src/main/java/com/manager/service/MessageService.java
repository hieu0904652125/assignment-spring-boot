package com.manager.service;


import com.manager.exception.LogicException;
import com.manager.model.Content;
import com.manager.model.Message;
import com.manager.repository.ContentRepository;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService implements IMessageService {
    @Autowired
    ContentRepository contentRepository;

    @Override
    public String convertMessage(String message) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        StringBuilder mess = new StringBuilder(message);
        for (int i = 0; i < message.length(); i++) {
            if (Character.isUpperCase(message.charAt(i))) {
                message.replaceAll(message.charAt(i) + "", "_" + Character.toLowerCase(message.charAt(i)));
            }
        }
        mess.insert(0, "%");
        mess.append("%");
        return mess.toString();
    }

    @Override
    public String convertValue( Message message,String contextId) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        if (!contextId.matches("-?\\d+(\\.\\d+)?")){
            throw new NullPointerException("Wrong Id!");
        }
        Content content =contentRepository.findById(Integer.parseInt(contextId)).orElseThrow(null);
        if(content==null){
            throw new NullPointerException("Wrong Id!");
        }
        String contentMess = content.getContent();
        StringBuilder stringBuilder = new StringBuilder(content.getContent());
        for (Field field : Message.class.getDeclaredFields()) {
            String param = convertMessage(field.getName());
            if (contentMess.contains(param)) {
                if(BeanUtils.getProperty(message,field.getName())==null){
                    throw new NullPointerException("data is empty");
                }
                stringBuilder.replace(0, stringBuilder.length(),
                        stringBuilder.toString().replace(param, BeanUtils.getProperty(message,field.getName())));
            }
        }
        return stringBuilder.toString();
    }
}



