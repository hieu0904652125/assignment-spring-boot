package com.manager.service;

import com.manager.exception.LogicException;
import com.manager.model.Help;
import com.manager.repository.HelpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

;
import java.util.List;


@Service

public class HelpService implements IHelpService {
    @Autowired
    HelpRepository helpRepository;

    @Override
    public List<Help> searchHelp(Integer staffId, Integer parentHelpId, String status, String type, Integer shopId) throws LogicException {
        if (staffId == null) {
            throw new LogicException("Staff ID cant be null");
        }
        List<Help> helpList = helpRepository.searchHelps(staffId, parentHelpId, status, type, shopId);
        return helpList;
    }


    @Override
    public Help addHelp(Help help) throws LogicException {
        if (help == null) {
            throw new LogicException("No data found");
        }
        helpRepository.save(help);
        return help;
    }

    @Override
    @Transactional(rollbackFor= {LogicException.class })
    public Help updateHelp(Help help, Integer helpId) throws LogicException {
        if (helpId == null) {
            throw new LogicException("Help ID cant be null");
        }
        if (help == null) {
            throw new LogicException("No data found");
        }
        Help helpById = helpRepository.findById(helpId).orElseThrow(null);
        if (helpById == null) {
            throw new LogicException("Wrong id!");
        }
        helpById.setHelpName(help.getHelpName());
        helpById.setParentHelpId(help.getParentHelpId());
        helpById.setType(help.getType());
        helpById.setPosition(help.getPosition());
        helpById.setContent(help.getContent());
        helpById.setStatus(help.getStatus());
        helpById.setCreatedUser(help.getCreatedUser());
        helpRepository.save(helpById);
        return helpById;

    }
    @Override
    @Transactional(rollbackFor= {LogicException.class })
    public boolean deleteHelp(Integer helpId) throws LogicException {
        if (helpId == null) {
            throw new LogicException("Fill your id!");
        }
        Help help = helpRepository.findById(helpId).orElse(null);
        if (help == null) {
            throw new LogicException("Wrong id!");

        }
        help.setStatus("0");
        helpRepository.save(help);
        return true;
    }

}
