package com.manager.service;

import com.manager.exception.LogicException;
import com.manager.model.Staff;

import java.util.List;

public interface IStaffService {
    public List<Staff> searchStaff(String staff_code, String staff_name, String tel, Integer shop_id,
                                   String id_no, String status, Integer help_id) throws LogicException;
}
