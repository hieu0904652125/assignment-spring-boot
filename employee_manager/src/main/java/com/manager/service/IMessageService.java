package com.manager.service;

import com.manager.exception.LogicException;
import com.manager.model.Message;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
public interface IMessageService {
    public String convertMessage(String message) throws  InvocationTargetException, IllegalAccessException, NoSuchMethodException;
    public String convertValue (Message message,String context) throws  InvocationTargetException, IllegalAccessException, NoSuchMethodException;
}
