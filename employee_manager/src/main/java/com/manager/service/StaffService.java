package com.manager.service;

import com.manager.exception.LogicException;
import com.manager.model.Staff;
import com.manager.repository.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class StaffService implements IStaffService {
    @Autowired
    StaffRepository staffRepository;
    @Autowired
    EntityManager entityManager;

    @Override
    public List<Staff> searchStaff(String staff_code, String staff_name, String tel, Integer shop_id, String id_no, String status, Integer help_id) throws LogicException {
        if(status==null ){
            throw new LogicException("status cant be null");
        }
        return staffRepository.searchStaffs(staff_code, staff_name, tel, shop_id, id_no, status, help_id);
    }
}
