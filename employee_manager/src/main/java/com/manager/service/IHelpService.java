package com.manager.service;

import com.manager.exception.LogicException;
import com.manager.model.Help;

import java.util.List;

public interface IHelpService {
    public List<Help> searchHelp(Integer staffId, Integer parentHelpId, String status, String type, Integer shopId) throws LogicException;
    public Help addHelp(Help help) throws LogicException;

    public Help updateHelp (Help employee, Integer helpId) throws LogicException;
    public boolean deleteHelp(Integer helpId) throws LogicException;

}
