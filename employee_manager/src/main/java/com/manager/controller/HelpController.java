package com.manager.controller;

import com.manager.exception.LogicException;
import com.manager.model.Help;
import com.manager.service.HelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/help")
public class HelpController {
    @Autowired
    HelpService helpService;

    @GetMapping("/search")
    public ResponseEntity<?> searchHelp(Integer staffId, Integer parentHelpId, String status, String type, Integer shopId){
        List<Help> helpList = null;
        try {
            helpList = helpService.searchHelp(staffId, parentHelpId, status, type, shopId);
        } catch (LogicException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(helpList);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addHelp(@RequestBody Help help) {
        try {
            Help resultHelp = helpService.addHelp(help);
        } catch (LogicException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok("Add successful!");
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateHelp(@RequestParam("id") Integer helpId, @RequestBody Help help) {
        Help resultHelp = null;
        try {
            resultHelp = helpService.updateHelp(help, helpId);
        } catch (LogicException e) {
             return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok("Update successfull!");
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteHelp(@RequestParam("id") Integer helpId) {
        try {
            boolean a=helpService.deleteHelp(helpId);
        } catch (LogicException e) {
             return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok("Delete successfully!");

    }

}
