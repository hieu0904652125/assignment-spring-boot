package com.manager.controller;

import com.manager.exception.LogicException;
import com.manager.model.Staff;

import com.manager.repository.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.manager.service.StaffService;

import java.util.List;

@RestController
@RequestMapping("/staff")
public class StaffController {

    @Autowired
    StaffService staffService;
    @Autowired
    StaffRepository staffRepository;

    @GetMapping("/search")
    public ResponseEntity<?> searchStaff(String code, String name, String tel, Integer shopId,
                                   String id, String status, Integer helpId) {
        List<Staff> staffList= null;
        try {
            staffList = staffService.searchStaff(code, name, tel, shopId, id, status, helpId);
        } catch (LogicException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST) ;
        }
        return ResponseEntity.ok(staffList);
    }
}                                                                           