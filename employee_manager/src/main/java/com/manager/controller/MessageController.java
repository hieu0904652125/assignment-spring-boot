package com.manager.controller;

import com.manager.model.Message;
import com.manager.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/message")
public class MessageController {
    @Autowired
    MessageService messageService;
    @GetMapping("/mess")
    public String message(String Id, @RequestBody Message message){
        try {
            String content= messageService.convertValue(message,Id);
            return content;
        }
        catch (Exception e){
            return e.getMessage();
        }
    }
}
