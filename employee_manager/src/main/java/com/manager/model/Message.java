package com.manager.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "message")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Message {
    @Id
    String messageOne;

    String messageTwo;

    String runDay;

    @Override
    public String toString() {
        return "Message{" +
                "messageOne='" + messageOne + '\'' +
                ", messageTwo='" + messageTwo + '\'' +
                ", runDay=" + runDay +
                '}';
    }
}
