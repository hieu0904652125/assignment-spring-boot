package com.manager.model;

import javax.persistence.*;

@Entity
@Table(name = "STAFF")
@SqlResultSetMapping(
        name = "StaffResult",
        classes = {
                @ConstructorResult(
                        targetClass = com.manager.model.Staff.class,
                        columns = {
                                @ColumnResult(name = "staff_id", type = Integer.class),
                                @ColumnResult(name = "staff_code"),
                                @ColumnResult(name = "staff_name"),
                                @ColumnResult(name = "tel"),
                                @ColumnResult(name = "address"),
                                @ColumnResult(name = "shop_id", type = Integer.class),
                                @ColumnResult(name = "id_no"),
                                @ColumnResult(name = "status"),
                        })})
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer staffId;
    private String staffCode;
    private String staffName;
    private String tel;
    private String address;
    private Integer shopId;
    private String idNo;
    private String status;

    @Override
    public String toString() {
        return "Staff{" +
                "staffId=" + staffId +
                ", staffCode='" + staffCode + '\'' +
                ", staffName='" + staffName + '\'' +
                ", tel='" + tel + '\'' +
                ", address='" + address + '\'' +
                ", shopId=" + shopId +
                ", idNo='" + idNo + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public Staff(){

    }

    public Staff(Integer staffId, String staffCode, String staffName, String tel, String address, Integer shopId, String idNo, String status) {
        this.staffId = staffId;
        this.staffCode = staffCode;
        this.staffName = staffName;
        this.tel = tel;
        this.address = address;
        this.shopId = shopId;
        this.idNo = idNo;
        this.status = status;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
