package com.manager.model;


import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "HELP")
@Data
@AllArgsConstructor  // khai bao constructor co tham so
@NoArgsConstructor // khai bao constructor k tham so
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@SqlResultSetMapping(
        name = "HelpResult",
        classes = {
                @ConstructorResult(
                        targetClass = com.manager.model.Help.class,
                        columns = {
                                @ColumnResult(name = "help_id", type = Integer.class),
                                @ColumnResult(name = "help_name"),
                                @ColumnResult(name = "parent_help_id", type = Integer.class),
                                @ColumnResult(name = "type"),
                                @ColumnResult(name = "position"),
                                @ColumnResult(name = "content"),
                                @ColumnResult(name = "status"),
                                @ColumnResult(name = "created_user"),
                        })})
public class Help {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "HELP_ID")
     int helpId;
    @Column(name = "HELP_NAME")
     String helpName;

     Integer parentHelpId;
     String type;
     String position;
     String content;
     String status;

     String createdUser;

    @Override
    public String toString() {
        return "Help{" +
                "helpId=" + helpId +
                ", helpName='" + helpName + '\'' +
                ", parentHelpId=" + parentHelpId +
                ", type='" + type + '\'' +
                ", position='" + position + '\'' +
                ", content='" + content + '\'' +
                ", status='" + status + '\'' +
                ", createdUser='" + createdUser + '\'' +
                '}';
    }
}
