package com.manager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class LogicException extends Exception{
    public LogicException() {
    }
    public LogicException(String var1) {
        super(var1);
    }

    public LogicException(String var1, Throwable var2) {
        super(var1, var2);
    }

    public LogicException(Throwable var1) {
        super(var1);
    }

    protected LogicException(String var1, Throwable var2, boolean var3, boolean var4) {
        super(var1, var2, var3, var4);
    }
}

