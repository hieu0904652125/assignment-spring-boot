package com.manager.repository;

import com.manager.model.Help;

import java.util.List;
public interface HelpRepositoryCustom {
    List<Help> searchHelps(Integer staffId, Integer parentHelpId, String status, String type, Integer shopId);
}
