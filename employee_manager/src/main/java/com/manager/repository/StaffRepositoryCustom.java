package com.manager.repository;

import com.manager.model.Staff;

import java.util.List;

public interface StaffRepositoryCustom {
    public List<Staff> searchStaffs(String staffCode, String staffName, String tel, Integer shopId,
                                   String idNo, String status, Integer helpId) ;
}
