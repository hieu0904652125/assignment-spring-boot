package com.manager.repository;

import com.manager.model.Help;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HelpRepository extends JpaRepository<Help,Integer>, HelpRepositoryCustom {

}
