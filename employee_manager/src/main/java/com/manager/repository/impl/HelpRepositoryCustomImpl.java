package com.manager.repository.impl;

import com.manager.model.Help;
import com.manager.repository.HelpRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class HelpRepositoryCustomImpl implements HelpRepositoryCustom {

    @Autowired
    EntityManager entityManager;

    @Override
    public List<Help> searchHelps(Integer staffId, Integer parentHelpId, String status, String type, Integer shopId) {
        StringBuilder sql = new StringBuilder();
        sql.append("select h.help_id,h.help_name,h.parent_help_id,h.type,h.position,h.content,h.status,h.created_user");
        sql.append(" from Help h, Staff s");
        sql.append(" where ( h.created_user = s.staff_code(+)) ");
        if (staffId != null) {
            sql.append(" and s.staff_id= TO_NUMBER(:staff_id) ");
        }
        if (parentHelpId != null) {
            sql.append(" and h.parent_help_id= TO_NUMBER(:parent_help_id)");
        }
        if (status != null) {
            sql.append(" and h.status= :status ");
        }
        if (type != null) {
            sql.append(" and h.type=:type ");
        }
        if (shopId != null) {
            sql.append(" and s.shop_id= TO_NUMBER(:shop_id)");
        }
        Query query = entityManager.createNativeQuery(sql.toString(),"HelpResult");
        if (staffId != null) {
            query.setParameter("staff_id", staffId);
        }
        if (parentHelpId != null) {
            query.setParameter("parent_help_id", parentHelpId);
        }
        if (status != null) {
            query.setParameter("status", status);
        }
        if (type != null) {
            query.setParameter("type", type);
        }
        if (shopId != null) {
            query.setParameter("shop_id", shopId);
        };
        return query.getResultList();
    }
}
