package com.manager.repository.impl;

import com.manager.model.Staff;
import com.manager.repository.StaffRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class StaffRepositoryCustomImpl implements StaffRepositoryCustom {
    @Autowired
    EntityManager entityManager;
    @Override
    public List<Staff> searchStaffs(String staffCode, String staffName, String tel, Integer shopId, String idNo, String status, Integer helpId) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT\n" +
                "    s.staff_id,\n" +
                "    s.staff_code,\n" +
                "    s.staff_name,\n" +
                "    s.tel,\n" +
                "    s.address,\n" +
                "    s.shop_id,\n" +
                "    s.id_no,\n" +
                "    s.status");
        sql.append(" FROM\n" +
                "    Help p,\n" +
                "    Staff s");
        sql.append(" WHERE\n" +
                "    ( p.created_user(+)  = s.staff_code )");
        if (staffCode !=null) {
            sql.append(" and (s.staff_code= :staff_code)");
        }
        if (staffName !=null){
            sql.append(" and s.staff_name like :staff_name \n");
        }
        if (tel !=null) {
            sql.append(" and s.tel = :tel ");
        }
        if (shopId != null) {
            sql.append(" and s.shop_id = TO_NUMBER (:shop_id)\n");
        }
        if (idNo !=null) {
            sql.append(" and s.id_no = TO_NUMBER(:id_no)\n");
        }
        if (helpId != null) {
            sql.append(" and p.help_id = TO_NUMBER(:help_id)) ");
        }
        if (status !=null) {
            sql.append("and s.status=:status ");
        }
        sql.append(" GROUP BY\n" +
                "    s.staff_id,\n" +
                "    s.staff_code,\n" +
                "    s.staff_name,\n" +
                "    s.tel,\n" +
                "    s.address,\n" +
                "    s.shop_id,\n" +
                "    s.id_no,\n" +
                "    s.status ");
        Query query = entityManager.createNativeQuery(sql.toString(),"StaffResult");
        if (staffCode !=null) {
            query.setParameter("staff_code", staffCode);}
        if (staffName !=null) {
            query.setParameter("staff_name", "%" + staffName + "%");}
        if (tel !=null) {
            query.setParameter("tel", tel);}
        if (shopId !=null) {
            query.setParameter("shop_id", shopId);}
        if (idNo !=null) {
            query.setParameter("id_no", idNo);}
        if (helpId !=null) {
            query.setParameter("help_id", helpId);}
        if (status !=null) {
            query.setParameter("status", status);}
        return query.getResultList();
    }
}
